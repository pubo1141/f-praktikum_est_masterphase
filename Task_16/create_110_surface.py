from pymatgen.core import Structure, Lattice
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen.core.surface import *


############## Input-parameters need to be changed here: ##############
a0 = 4.0834
alpha = 60.0
kpoints_bulk = 6
elements_prim = ["Ga", "As"]
coords_prim = [(0.0, 0.0, 0.0), (0.75, 0.75, 0.75)]
#######################################################################

# Generate the primitive bulk crystal structure:
bulk_lattice_prim = Lattice.from_parameters(
    a=a0, b=a0, c=a0, alpha=alpha, beta=alpha, gamma=alpha
)
bulk_crystal_prim = Structure(bulk_lattice_prim, elements_prim, coords_prim)
bulk_crystal_prim.to("bulk_prim.xsf")

# Transform the crystal structure into the conventional cell:
sg_analyzer = SpacegroupAnalyzer(bulk_crystal_prim, symprec=0.1)
bulk_crystal_conv = sg_analyzer.get_conventional_standard_structure()
print(sg_analyzer.get_space_group_number())
bulk_crystal_conv.to("bulk_conv.xsf")
print(bulk_crystal_conv)

# Build up the surface-slabs with the slab-generator:
miller_indices = (1, 1, 0)
min_slab_size = 9
min_vacuum_size = 9

slab_gen = SlabGenerator(
    initial_structure=bulk_crystal_conv,
    miller_index=miller_indices,
    min_slab_size=min_slab_size,
    min_vacuum_size=min_vacuum_size,
    center_slab=True,
    reorient_lattice=True,
    max_normal_search=15,
    in_unit_planes=True,
)
slabs = slab_gen.get_slabs()

# Print the number of terminations that could be found:
print(f"Number of terminations {len(slabs)}")
slab = slabs[0]
print(slab)
slab.to("110_surf_pymatgen.xsf")

# Reading the template-file and adapting the content
# and storing it as a list of lines in "input_content".
input_content = []
with open("template.inp", "r") as temp_content:
    for line in temp_content:
        if "&CELL" in line:
            input_content.append(line)
            for dir, vector in zip(["A", "B", "C"], slab.lattice.matrix):
                vector_str = [str(round(vec, 4)) for vec in vector]
                input_content.append(f"      {dir} " + " ".join(vector_str) + "\n")
        elif "&COORD" in line:
            input_content.append(line)
            for ii_at in range(len(slab)):
                position = slab.sites[ii_at].coords
                position_str = [str(round(pos, 4)) for pos in position]
                input_content.append(
                    f"      {slab.sites[ii_at].specie} " + " ".join(position_str) + "\n"
                )
        elif "SCHEME MONKHORST-PACK" in line:
            # We scale the k-points based on the cell dimensions with the constraint
            # that we would like to have an even number in each direction:
            cell_lengths = slab.lattice.abc
            kpoints_slab = []
            for idx in range(2):
                kpoint = math.ceil(round(a0 * kpoints_bulk / cell_lengths[idx], 2))
                kpoint = kpoint + kpoint % 2
                kpoints_slab.append(str(kpoint))
            input_content.append(
                "         SCHEME MONKHORST-PACK " + " ".join(kpoints_slab) + " 1\n"
            )
        else:
            input_content.append(line)

# Create the input-file for calculation:
with open("surface-geo_opt.inp", "w") as input_file:
    for line in input_content:
        input_file.write(line)
