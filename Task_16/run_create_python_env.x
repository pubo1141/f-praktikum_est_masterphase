#!/bin/bash

ml Python/3.11.3-GCCcore-13.1.0

python -m venv ./python_env
source python_env/bin/activate
pip install pymatgen==2024.4.13 > create_python_env.log 2>& 1&
