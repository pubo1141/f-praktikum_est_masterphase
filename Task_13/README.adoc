= Convergence of the k-point mesh in CP2K

[NOTE]
===============================
*Table of Contents*

* General introduction to the convergence parameters
* Practical calculations
===============================

== Some general comments

When calculating periodic systems the number and distribution of k-points that are used to sample the 1st Brillouin zone need to be tweaked.
Since the Brillouin zone represents the reciprocal lattice of the real space, a larger number of k-points are needed for small unit cells, while very large unit cells can be well approximated by the Gamma-point only.

Depending on the symmetry of the crystal, certain types of k-meshes can converge faster than others.
For example for cubic crystals a mesh excluding the Gamma-point is favourable since this high-symmetry point in the Brillouin zone can strongly affect the results.
Choosing a mesh with an even number of points for each direction or a mesh with an offset will remedy this effect.
On the other hand, crystals that have a hexagonal symmetry should always be calculated including the Gamma-point in the k-mesh.
The reason for this is that the hexagonal symmetry operations would otherwise create points outside the Brillouin zone.


Another important aspect to consider is the smoothness of the Fermi-surface.
In general, semiconductors have a smoother Fermi-surface than metals and, subsequently, less k-points are needed to accurately describe the system.

== Practical calculations

We will pursue the same route as in the last Section: the total energy of the bulk unit cell is computed with respect to the number of k-points.
We will choose the k-mesh where the total energy is converged below latexmath:[10^{-4}] Hartree.

The header of the file is the same as in link:../Section_22/[Section 2.2]

[source,bash]
....
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=24
#SBATCH -p carl.p
#SBATCH --time=1-00:00:00

ml hpc-env/8.3 
ml CP2K/7.1-intel-2019b

export OMP_NUM_THREADS=1


kpoints="2 4 6 8 10 12"

template_file=template.inp
input_file=energy_kpoints
plot_file=kpoints_data.ssv
cp2k_bin=cp2k.popt

# Some headers for the plot-file
echo "# k-points vs total energy" > $plot_file
echo "# Date: $(date)" >> $plot_file
echo "# PWD: $PWD" >> $plot_file
echo "# Nr. k-points | Total Energy (Ha)" >> $plot_file
....

We will iterate over a list of k-point meshes ranging from 2 to 12 k-points for each dimension:

[source,bash]
....
for kpoint in $kpoints ; do
    # Creation of input files
    echo "Running for kpoints = $kpoint "
    work_dir=kpoints_${kpoint}x${kpoint}x${kpoint}
    if [ ! -d $work_dir ] ; then
        mkdir $work_dir
    else
        rm -r $work_dir/*
    fi
    sed -e "s/LT_kpoints/${kpoint}/g" \
        $template_file > $work_dir/${input_file}.inp

    # Now we start the calculation
    cd $work_dir
    mpirun ${cp2k_bin} -i ${input_file}.inp >& ${input_file}.out
    cd ..

    # Postprocessing
    total_energy=$(grep -e '^[ \t]*Total energy' $work_dir/${input_file}.out | awk '{print $3}')
    printf "%2s  %15.10f" $kpoint $total_energy >> $plot_file
    printf "\n" >> $plot_file
done
....

Likwise to last Section, the script is run with the command "sbatch run_kpoints.job".
The content of the output-file "kpoints_data.ssv" for this example is given below:

....
# k-points vs total energy
# Date: Thu Apr 22 09:12:57 CEST 2021
# PWD: /user/pubo1141/F-Praktikum_SS21/Section_23/example
# Nr. k-points | Total Energy (Ha)
 2   -80.9185445078
 4   -80.9245942766
 6   -80.9247456685
 8   -80.9247558868
10   -80.9247567772
12   -80.9247568057
....

We are applying the same convergence criteria as in the last Section, changes in the total energy should be less than latexmath:[10^{-4}] Hartree.
In this case we choose a k-point mesh of 6x6x6 points.
